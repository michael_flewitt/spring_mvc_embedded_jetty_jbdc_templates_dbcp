This project uses Spring 5.1.6 MVV as the base library.
The application has simple routes to test inserts into a database with various field types.
The server is run using an embedded jetty server
Database queries are run through the JDBC Template object.
The datasource configured is a DBCP2 source and all JDBC transactions are routed through DBCP2

Included is also a jmx file, to simulate test load in jmeter.

To run the server use "mvn jetty:run"