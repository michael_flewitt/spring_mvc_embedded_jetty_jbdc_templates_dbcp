package com.intergral.mflewitt.controller;

import com.intergral.mflewitt.dao.TestTableDAO;
import com.intergral.mflewitt.model.TestTable;
import java.sql.Date;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BasicController {

    @Autowired
    TestTableDAO dao;
    Random rand = new Random();

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String home() {
        return "index";
    }

    @GetMapping(value = "/find", produces = MediaType.TEXT_HTML_VALUE)
    public String findById()
    {
        int r = rand.nextInt(10) + 1;
        System.out.println("-------------------------------------");
        System.out.println("find");
        System.out.println(dao.findByID( 2 ));
        return "index";
    }

    @GetMapping(value = "/error", produces = MediaType.TEXT_HTML_VALUE)
    public String error()
    {
        int r = rand.nextInt(10) + 1;
        System.out.println("-------------------------------------");
        System.out.println("error");
        System.out.println(dao.findError());
        return "index";
    }

    @GetMapping(value = "/insert", produces = MediaType.TEXT_HTML_VALUE)
    public  String insert()
    {
        int r = rand.nextInt(10) + 1;
        System.out.println("-------------------------------------");
        System.out.println("insert");
        TestTable test = new TestTable( (int) Math.random(), "hello", "hello", 1, 2.1 );
        System.out.println(dao.insert( test ));
        return "index";
    }
}