package com.intergral.mflewitt.dao;

import com.intergral.mflewitt.model.TestTable;
import com.intergral.mflewitt.model.TestTableMapper;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class TestTableDAO implements ITestTableDAO
{
    JdbcTemplate template;

    private final String SQL_FIND_BY_ID = "SELECT * FROM TestTable WHERE id = ?";
    private final String SQL_INSERT = "INSERT INTO TestTable (id, String1, String2, Num1, Double1 ) VALUES (?, ?, ?, ?, ?)";
    private final String SQL_ERROR = "SELECT * FROM error";

    @Autowired
    public TestTableDAO(DataSource dataSource)
    {
        template = new JdbcTemplate( dataSource );
    }


    public int insert( final TestTable testTable )
    {
        return template.update( SQL_INSERT, testTable.getId(), testTable.getString1(), testTable.getString2(), testTable.getNum1(), testTable.getDouble1() );
    }


    public TestTable findByID( final int id )
    {
        return template.queryForObject( SQL_FIND_BY_ID, new Object[] {id}, new TestTableMapper() );
    }

    public int findError()
    {
        template.execute( SQL_ERROR );
        return 1;
    }
}