package com.intergral.mflewitt.dao;


import com.intergral.mflewitt.model.TestTable;

public interface ITestTableDAO
{
    public int insert( TestTable testTable);
    public TestTable findByID(int id);
}
